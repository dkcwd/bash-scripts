#!/bin/bash
sudo -v

echo "Creating a $HOME/Projects folder if it doesn't already exist"
sudo mkdir -p $HOME/Projects

echo "Cloning ZF2 Skeleton Application from github.com as $1"
cd $HOME/Projects
sudo git clone https://github.com/zendframework/ZendSkeletonApplication.git $1

echo "Altering project file permissions to make $USER owner of the project files"
sudo chown $USER -R $1
cd $1

echo "Running composer self-update"
sudo php composer.phar self-update

echo "Updating dependencies"
sudo php composer.phar update

echo "Creating a vhost for $1 with a webroot in $HOME/Projects/$1/public/"
sudo cp /etc/apache2/sites-available/zf2-template /etc/apache2/sites-available/$1

echo "Updating vhost template variable with project specific values"
sudo sed -i 's/zf2-template/'$1'/g' /etc/apache2/sites-available/$1
sudo sed -i 's/username/'$USER'/g' /etc/apache2/sites-available/$1

echo "Adding $1 to hosts file"
sudo sed -i '1s/^/127.0.0.1       '$1'.local\n/' /etc/hosts

echo "Enabling vhost"
sudo a2ensite $1

echo "Reloading Apache config"
sudo service apache2 reload

echo "You should be able to see the project at http://$1.local"
