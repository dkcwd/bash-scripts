if [ "" = "$1" ]
 then
  echo "Error: A value for the project host name must be set";
  exit 1
fi

if [ "zf2-template" = "$1" ]
 then
  echo "Error: A value for the project host name must be set which is not 'zf2-template'";
  exit 1
fi

if [ "" = "$2" ]
 then
  echo "Error: A valid path to the project public folder must be provided";
  exit 1
fi

sudo rm -rf /etc/apache2/sites-available/$1.conf

echo "value for the web root $2";

echo "Creating a vhost for $1 with a webroot in $2"
sudo cp /etc/apache2/sites-available/zf2-template.conf /etc/apache2/sites-available/$1.conf

echo "Updating vhost template variable with project specific values"
sudo sed -i 's/zf2-template/'$1'/g' /etc/apache2/sites-available/$1.conf
sudo sed -i 's/project-web-root/'$2'/g' /etc/apache2/sites-available/$1.conf

echo "Adding $1 to hosts file"
sudo sed -i '1s/^/127.0.0.1       '$1'.local\n/' /etc/hosts

echo "Enabling vhost"
sudo a2ensite $1.conf

echo "Reloading Apache config"
sudo service apache2 reload
